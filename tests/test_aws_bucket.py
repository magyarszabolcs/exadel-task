from __future__ import annotations

import pytest
from botocore.client import ClientError
from moto import mock_s3

from src.aws_bucket import AWSS3Bucket


@mock_s3
def test_bucket_creation():
    s3_bucket = AWSS3Bucket(bucket_name='mybucket', region='eu-west-3')

    bucket_created = s3_bucket.create_s3_bucket()
    assert 200 == bucket_created['ResponseMetadata']['HTTPStatusCode']


@mock_s3
def test_bucket_creation_twice_with_same_name():
    with pytest.raises(ClientError):
        s3_bucket = AWSS3Bucket(bucket_name='mybucket', region='eu-west-3')

        s3_bucket.create_s3_bucket()
        s3_bucket.create_s3_bucket()


@mock_s3
def test_bucket_creation_invalid_region():
    """This should raise a ClientError because of the wrong region,
    but it does not do because of moto."""
    s3_bucket = AWSS3Bucket(bucket_name='mybucket', region='eu-westts-3')

    bucket_created = s3_bucket.create_s3_bucket()
    assert 200 == bucket_created['ResponseMetadata']['HTTPStatusCode']
