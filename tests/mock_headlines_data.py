from __future__ import annotations
data = {
    'status': 'ok',
    'totalResults': 37,
    'articles': [
        {
            'source': {'id': 'the-washington-post', 'name': 'The Washington Post'},
            'author': 'Jennifer Hassan',
            'title': 'South Africa investigates mysterious nightclub deaths at Enyobeni Tavern - The Washington Post',
            'description': 'The youngest victim at the Enyobeni Tavern in the city of East London was 13, police said.',
            'url': 'https://www.washingtonpost.com/world/2022/06/27/south-africa-nightclub-deaths-east-london/',
            'urlToImage': 'https://www.washingtonpost.com/wp-apps/imrs.php?src=https://arc-anglerfish-washpost-prod-washpost.s3.amazonaws.com/public/F2QRO2XVOAI6ZAO3VQD2HFFINM.jpg&w=1440',
            'publishedAt': '2022-06-27T14:18:00Z',
            'content': 'Placeholder while article actions load\r\nPolice in South Africa are investigating the deaths of at least 21 young people whose bodies were found inside a nightclub in the city of East London over the … [+2434 chars]',
        },
        {
            'source': {'id': 'abc-news', 'name': 'ABC News'},
            'author': 'ABC News',
            'title': "Brittney Griner appears at preliminary hearing amid 'wrongful' detention in Russia - ABC News",
            'description': 'Brittney Griner appeared at a preliminary hearing in Moscow on Monday more than three months after the WNBA star was detained in Russia.',
            'url': 'https://abcnews.go.com/International/brittney-griners-preliminary-hearing-moscow-scheduled-monday/story?id=85753028',
            'urlToImage': 'https://s.abcnews.com/images/International/griner-court-hearing-gty-ps-220627_1656334690022_hpMain_16x9_992.jpg',
            'publishedAt': '2022-06-27T14:15:00Z',
            'content': "WNBA star Brittney Griner appeared at a preliminary hearing in Moscow on Monday more than three months after she was detained in Russia.\r\nGriner did not respond to an ABC News reporter's question as … [+1608 chars]",
        },
    ],
}
