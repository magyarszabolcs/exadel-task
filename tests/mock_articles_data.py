from __future__ import annotations
data = [
    {
        'source': {'id': 'abc-news', 'name': 'ABC News'},
        'author': 'ABC News',
        'title': "Brittney Griner appears at preliminary hearing amid 'wrongful' detention in Russia - ABC News",
        'description': 'Brittney Griner appeared at a preliminary hearing in Moscow on Monday more than three months after the WNBA star was detained in Russia.',
        'url': 'https://abcnews.go.com/International/brittney-griners-preliminary-hearing-moscow-scheduled-monday/story?id=85753028',
        'urlToImage': 'https://s.abcnews.com/images/International/griner-court-hearing-gty-ps-220627_1656334690022_hpMain_16x9_992.jpg',
        'publishedAt': '2022-06-27T14:15:00Z',
        'content': "WNBA star Brittney Griner appeared at a preliminary hearing in Moscow on Monday more than three months after she was detained in Russia.\r\nGriner did not respond to an ABC News reporter's question as … [+1608 chars]",
    },
]
