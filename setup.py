from __future__ import annotations

from setuptools import setup

with open('README.md') as f:
    long_description = f.read()

setup(
    name='exadel-task-szabolcs-magyar',
    version='1.0',
    description='A module created for solving the task given by Exadel',
    license='MIT',
    long_description=long_description,
    author='Szabolcs Magyar',
    author_email='magyarszabolcs94@gmail.com',
    packages=['exadel-task-szabolcs-magyar'],
)
