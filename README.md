# Exadel task Szabolcs Magyar

The main purpose of this repository is to show a possible solution for a task provided by Exadel.

## Description

The task was to query the english sources from NewsAPI as a first step. For each source, top headlines needed to be collected from the same API. These top headlines should be uploaded to an AWS S3 bucket.

## Getting Started

### Dependencies

For using NewsAPI, please create a free account at https://newsapi.org/ and generate an API key.

For accessing the AWS S3 bucket, generate the AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY. There is a documentation on how to do it at https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html#cli-configure-quickstart-creds-create

### Execution

To run the pipeline, you need to have Docker Desktop running, which you can download from here: https://www.docker.com/products/docker-desktop/.

The region set currently is "eu-west-3". You can change this by editing the AWS_REGION and REGION variables in the Dockerfile.

Once Docker Desktop has started running, you can build the image with the following command:

```
docker build -t exadel --build-arg AWS_ACCESS_KEY_ID=<insert-your-aws-access-key-id-here> --build-arg AWS_SECRET_ACCESS_KEY=<insert-your-aws-secret-access-key-here> .
```

After that, to execute the pipeline, you need to run:
```
docker run -e API_KEY=<insert-your-newsapi-api-key-here> exadel
```

Please be aware that due to NewsAPI constraints, you can make only 50 requests as a free user.

## Tests

Pytest is used for testing purposes. To run the tests, run:

```
python -m pytest tests
```

The upload to S3 was not tested due to an Internal Error from AWS (aiobotocore is not compatible with moto).

## Chore

The repository uses pre-commit hooks to make the code standard. This configuration can be found in .pre-commit-config.yaml.

Before commiting, you need to run ```pre-commit run --all-files```, and correct the issues if you want to make changes in the codebase.

## Possible future improvements

- Creating the AWS S3 bucket through Terraform
- Increasing test coverage by e.g. mocking NewsAPI
- Adding CI/CD with three steps:
   1. Building Docker container
   2. Checking style and tests
   3. Docker run to run the pipeline
- Using Airflow as the pipeline orchestrator
