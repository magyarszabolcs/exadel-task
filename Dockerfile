FROM python:3.10

WORKDIR /

COPY requirements.txt requirements.txt

RUN pip3 install -r requirements.txt

COPY . .

ENV BUCKET_NAME="exadel-task"
ENV REGION="eu-west-3"

ARG AWS_ACCESS_KEY_ID
ENV AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID

ARG AWS_SECRET_ACCESS_KEY
ENV AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY

ENV AWS_REGION="eu-west-3"

RUN aws ecr get-login --no-include-email | bash

CMD [ "python3", "./src/__main__.py"]
