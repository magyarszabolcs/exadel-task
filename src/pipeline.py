from __future__ import annotations

import logging
import os

import botocore
from aws_bucket import AWSS3Bucket
from dataframe_operations import PandasDataframeOperations
from dotenv import load_dotenv
from newsapi import NewsApiClient
from newsapi_wrapper import NewsAPIWrapper


def pipeline():
    load_dotenv()

    API_KEY = os.environ.get('API_KEY')
    BUCKET_NAME = os.environ.get('BUCKET_NAME')
    REGION = os.environ.get('REGION')

    aws_s3_bucket = AWSS3Bucket(bucket_name=BUCKET_NAME, region=REGION)
    try:
        aws_s3_bucket.create_s3_bucket()
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == 'BucketAlreadyOwnedByYou':
            logging.warning('Bucket has already been created')
            # Ignored: the bucket does not need to be recreated every time
            pass

    newsapi = NewsApiClient(api_key=API_KEY)
    newsapi_wrapper = NewsAPIWrapper(newsapi)

    source_ids = newsapi_wrapper.return_source_ids_for_one_language(
        language='en',
    )

    for source in source_ids:
        articles = newsapi_wrapper.get_top_headline_articles_per_source(
            source=source,
        )

        articles_per_source = PandasDataframeOperations(
            articles_per_source=articles,
        )
        articles_per_source_df = (
            articles_per_source.convert_dict_to_pandas_dataframe()
        )

        articles_per_source.upload_dataframe_to_s3_bucket(
            articles_df=articles_per_source_df, bucket_name=BUCKET_NAME,
            source=source,
        )
