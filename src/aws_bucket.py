from __future__ import annotations

import boto3


class AWSS3Bucket:
    def __init__(self, bucket_name: str, region: str):
        self.bucket_name = bucket_name
        self.region = region

    def create_s3_bucket(self):
        s3 = boto3.client('s3')
        return s3.create_bucket(
            Bucket=self.bucket_name,
            CreateBucketConfiguration={'LocationConstraint': self.region},
        )
