from __future__ import annotations

import logging

from pipeline import pipeline

if __name__ == '__main__':
    pipeline()
    logging.info('The files have been successfully uploaded to S3')
