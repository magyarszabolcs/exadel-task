from __future__ import annotations

from time import time

import pandas as pd


class PandasDataframeOperations:
    def __init__(self, articles_per_source: list[dict]):
        self.articles_per_source = articles_per_source

    def convert_dict_to_pandas_dataframe(self) -> pd.DataFrame:
        articles_data = pd.json_normalize(self.articles_per_source)
        return articles_data.replace('\n', ' ', regex=True).replace(
            '\r', ' ', regex=True,
        )

    def upload_dataframe_to_s3_bucket(
        self, articles_df: pd.DataFrame, bucket_name: str, source: str,
    ) -> None:
        file_name = f'{time()}_headlines.csv'
        return articles_df.to_csv(
            f's3://{bucket_name}/{source}/{file_name}', index=False,
        )
