from __future__ import annotations

from newsapi import NewsApiClient


class NewsAPIWrapper:
    def __init__(self, news_api: NewsApiClient):
        self.newsapi = news_api

    def return_source_ids_for_one_language(self, language: str) -> list:
        sources = self.newsapi.get_sources(language=language)

        return [key['id'] for key in sources['sources']]

    def get_top_headline_articles_per_source(self, source: str) -> dict:
        top_headlines = self.newsapi.get_top_headlines(sources=source)
        return top_headlines['articles']
